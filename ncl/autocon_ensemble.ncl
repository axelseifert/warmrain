;================================================;
; These files are loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"   
; ================================================;
load "NCL/ucla_code.ncl"

begin

  if (.not.isvar("prefix")) then
    prefix = "boxwd"
  end if
  if (.not.isvar("basedir")) then
    basedir   = "../experiments/"
  end if

;=================================================;

; file output
  
  fodir  = basedir+"/Plots/"
  foform = "pdf"
  foname = "autocon"

;=================================================;

  lwrite_full_netcdf = True

; define phase space for training and testing data

  ltraining = True
  ltesting  = False
  lsmall    = True

  ltimescales = False   ; calculate only the conversion time scale and write separate netcdf

  if (ltraining) then
    if (lsmall) then
;     training small
      lwc0 = (/400,600,800,1000,2000/)
      rc0  = (/11,12,13,14,15/)
      nu0  = (/0,1,2/)
      pfix = "_train_small"
      ndim = 2     ; number of ensemble datasets
    else    
;     training big
      lwc0 = (/200,400,600,800,1000,2000/)
      rc0  = (/9,10,11,12,13,14,15/)
      nu0  = (/0,1,2,3,4/)    
      pfix = "_train_big"
      ndim = 5     ; number of ensemble datasets
    end if
  else 
    if (ltesting) then
      if (lsmall) then
;       testing small
        lwc0 = (/500,700,900,1500/)
        rc0  = (/11,12,13,14,15/)
        nu0  = (/0,1,2/)    
        pfix = "_test_small"
        ndim = 1
      else
;       testing big
        lwc0 = (/300,500,700,900,1500/)
        rc0  = (/9,10,11,12,13,14,15/)
        nu0  = (/0,1,2,3,4/)    
        pfix = "_test_big"
        ndim = 5
      end if
    else
      if (ltimescales) then
;       t10 and t50
        lwc0 = (/200,300,500,700,1000,1500,2000/)
        rc0  = (/10,11,12,13,14,15/)
        nu0  = (/0,1,2,3,4/)    
        pfix = "_t10"
        ndim = 5      ; number of ensemble datasets
      else
;       just plotting
        lwc0 = (/200,300,500,700,1000,1500,2000/)
        rc0  = (/11,12,13,14/)
        nu0  = (/0,1,2,3/)    
        pfix = ""
        ndim = 1
      end if
    end if
  end if

  xi   = 1024

  colors_lwc = (/30,50,78,120,140,150,160,175,190/)
  colors_nu  = (/30,50,78,120,140,150,160,175,190/)
  markers_nu = (/10,11,12,13,14,15,16/)
  colors_rc  = (/40,78,120,160,185/)
 
; some parameters 

  pi   = 3.14159265358979323846264338327
  rhow = 1e3

  kcc = 9.44e9     ; Long kernel in m3 kg-2 s-1 
  kcr = 5.78       ; Long kernel in m3 kg-1 s-1 

  xc0 = pi*rhow/6. * (rc0*2e-6)^3   ; initial mean mass of cloud droplets

  xstar = 2.6e-10   ; xstar in kg

; prepare arrays

  idim = dimsizes(lwc0)
  jdim = dimsizes(rc0)
  kdim = dimsizes(nu0)
  tdim = 4000
  dims = (/ndim,idim,jdim,kdim,tdim/)

  time = new( dims, "float")
  Lc = new( dims, "float")
  Nc = new( dims, "float")
  Zc = new( dims, "float")
  Lr = new( dims, "float")
  Nr = new( dims, "float")
  Zr = new( dims, "float")
  auto_num   = new( dims, "float")
  auto_mass  = new( dims, "float")
  accr_num   = new( dims, "float")
  accr_mass  = new( dims, "float")
  self_cloud = new( dims, "float")
  self_rain  = new( dims, "float")

  Lc_norm = new( dims, "float")
  Lr_norm = new( dims, "float")
  time_norm = new( dims, "float")
  auto_norm = new( dims, "float")
  accr_norm = new( dims, "float")

  t10 = new( (/ndim,idim,jdim,kdim/), "float")
  t50 = new( (/ndim,idim,jdim,kdim/), "float")

  nu4d = new( dims, "float")

; open file and read in data

  filename = "autocon.dat"
  do n=0,ndim-1
    do i=0,idim-1
      do j=0,jdim-1
        do k=0,kdim-1
        
          lwc = lwc0(i)
          rm  = rc0(j)
          nu  = nu0(k)

          ensdir = "data"+sprinti("%2.2i",n+1)
          expdir = prefix+"_lwc"+sprinti("%4.4i",lwc)+"_rm"+sprinti("%2.2i",rm) \
                         +"_nu"+sprinti("%2.2i",toint(nu*10))+"_xi"+sprinti("%4.4i",xi)

          filedir  = basedir+ensdir+"/"+expdir+"/"

;         some old ncl versions do not have fileexists(), use systemfunc instead
          ret = systemfunc("test -d "+filedir+"; echo $?")

          if (ret.eq.1) then
            print("Warning: Directory "+filedir+" not found.")
            continue ; cylce
          end if

          print("Reading "+filedir+filename)
          ncol = 14                             ; number of data columns in autocon.dat files
          nrow = numAsciiRow(filedir+filename)  ; number of rows in file
  
          data  := asciiread(filedir+filename,-1,"float")
          data := data(5:)  
        
          ntime = nrow-3

          if (dimsizes(data)/ncol.ne.ntime) then
            print("Error: something wrong with autocon.dat")
            print("  ncol  = "+ncol)
            print("  nrow  = "+nrow)
            print("  ntime = "+ntime)
            exit
          end if

          data := onedtond(data,(/ntime,ncol/))

          time(n,i,j,k,0:ntime-1) = data(:,0)
          Lc(n,i,j,k,0:ntime-1) = data(:,1)
          Nc(n,i,j,k,0:ntime-1) = data(:,2)
          Zc(n,i,j,k,0:ntime-1) = data(:,3)
          Lr(n,i,j,k,0:ntime-1) = data(:,4)
          Nr(n,i,j,k,0:ntime-1) = data(:,5)
          Zr(n,i,j,k,0:ntime-1) = data(:,6)
          auto_num(n,i,j,k,0:ntime-1)   = data(:,7)
          auto_mass(n,i,j,k,0:ntime-1)  = data(:,8)
          accr_num(n,i,j,k,0:ntime-1)   = data(:,9)
          accr_mass(n,i,j,k,0:ntime-1)  = data(:,10)
          self_cloud(n,i,j,k,0:ntime-1) = data(:,11)
          self_rain(n,i,j,k,0:ntime-1)  = data(:,12)
          
          L0 = lwc*1e-6
          Lc_norm(n,i,j,k,:) = Lc(n,i,j,k,:) / L0
          Lr_norm(n,i,j,k,:) = Lr(n,i,j,k,:) / L0
          time_norm(n,i,j,k,:) = time(n,i,j,k,:) * L0

          nu4d(n,i,j,k,0:ntime-1)  = nu

;         pre- and normalization factor for autoconversion based on Long kernel
;         (Equation 16 of SB2001)        
 
          auto_fac = kcc/(20*xstar) * (nu+2.0)*(nu+4.0)/(nu+1.0)^2 * L0^2 * xc0(j)^2

;         xc = Lc(n,i,j,k,:)/Nc(n,i,j,k,:)
;         auto_fac = kcc/(20*xstar) * (nu+2.0)*(nu+4.0)/(nu+1.0)^2 * Lc(n,i,j,k,:)^2 * xc0(j)^2

          auto_norm(n,i,j,k,:) = auto_mass(n,i,j,k,:) / auto_fac

          accr_fac = kcr * Lc(n,i,j,k,:) * Lr(n,i,j,k,:)
          accr_fac = where(accr_fac.gt.0,accr_fac,accr_fac@_FillValue)
          accr_norm(n,i,j,k,:) = accr_mass(n,i,j,k,:) / accr_fac

          tt := data(:,0)
          qc := data(:,1)
          qr := data(:,4)

;         check that the simulation has the correct initial mass
          if ( abs(L0-max(qc)).gt.1e-5) then
            print(" L0     = "+lwc)
            print(" max qc = "+max(qc))
          end if

;         calculate t10 and t50
          t10(n,i,j,k) = max(tt(ind(qr.le.0.1*L0)))
          t50(n,i,j,k) = max(tt(ind(qr.le.0.5*L0)))
;         print(" t10 = "+t10(n,i,j,k)+", t50 = "+t50(n,i,j,k))
          
        end do
      end do
    end do
  end do

;=================================================;

  wks  = gsn_open_wks(foform,fodir+foname+pfix)
  uclales_define_colormap(wks,"BkBlAqGrYeOrReViWh200+grey")
; gsn_define_colormap(wks,"BkBlAqGrYeOrReViWh200")

;=================================================;

; estimate shape parameter of raindrop gamma distribution

  gg  := todouble(where(Lc.gt.1e-10,Lc,Lc@_FillValue))
  gg  := where(Lc.gt.1e-10,todouble(Nc*Zc)/gg^2,gg@_FillValue)
  nue := (gg-2.0)/(1.0-gg)

; estimate shape parameter of raindrop gamma distribution

  gg  := todouble(where(Lr.gt.1e-10,Lr,Lr@_FillValue))
  gg  := where(Lr.gt.1e-10,todouble(Nr*Zr)/gg^2,gg@_FillValue)
  mue := (gg-20)*(0.5569344 + 0.03052414*gg - 0.000936101*gg^2)/(1 - 1.078084*gg)

  print(" min nue = "+min(nue)+" max nue = "+max(nue))
  print(" min mue = "+min(mue)+" max mue = "+max(mue))

;=================================================;

; prepare data: units, normalization etc.

  time = time / 60.  ; time 
  time@units = "min"

  plist = [/ Lc, Lr/]
  do n=0,ListCount(plist)-1 
    plist[n] = plist[n] * 1e3
    plist[n]@units = "g/m~S~3"
  end do

  plist = [/ Nc, Nr/]
  do n=0,ListCount(plist)-1 
    plist[n]@units = "m~S~-3"
  end do

  Lr_norm@units = ""

  tau = Lr / (Lc+Lr)   ; liquid water time scale

  tau = where(tau.le.0.999,tau,tau@_FillValue) ; taking out some values near tau=1

; initial cloud reflectivity
  Zc0 = conform(Zc,Zc(:,:,:,:,0),(/0,1,2,3/))

; some cloud reflectivity quantities
  Zc_norm = Zc / Zc0
  Zc_enh = (Zc-Zc0) / Zc0 * 100
  
  Zc_norm@long_name = "normalized cloud reflectivity Zc/Zc0"
  Zc_enh@long_name  = "cloud reflectivity enhancement factor (Zc-Zc0)/Zc0"
  Zc_norm@units = "1"
  Zc_enh@units  = "%"
  
;=================================================;

  items_lwc0 = ispan(0,idim-1,1)
  items_rc0  = ispan(0,jdim-1,1)
  items_nu0  = ispan(0,kdim-1,1)
  items_lwc0 = items_lwc0(::-1)
  items_rc0  = items_rc0(::-1)
  items_nu0  = items_nu0(::-1)

  res = True
  res@gsnDraw        = True
  res@gsnFrame       = False                    
  res@pmLegendDisplayMode    = "Always"            ; turn on legend
  res@pmLegendSide           = "Top"               ; Change location of 
  res@pmLegendParallelPosF   = 0.78                ; move units right
  res@pmLegendOrthogonalPosF = -0.47               ; move units down
  res@pmLegendWidthF         = 0.05                ; Change width and
  res@pmLegendHeightF        = 0.20                ; height of legend.
  res@lgPerimOn              = False               ; turn off box around
  res@lgLabelFontHeightF     = .018                ; label font height
  res@xyExplicitLegendLabels = "  L~B~0~N~ = "+lwc0*1e-3+" g m~S~-3"
  res@lgItemOrder            = items_lwc0
  res@tmXTOn = False
  res@tmYROn = False

  res@xyDashPatterns         = (/0,1,2,3,4,5,6,7/)
  res@xyLineThicknesses      = (/1,1,1,1,1,1,1,1/) * 3.
  res@xyLineColors           = colors_lwc

; subtitle
  txres                    = True
  txres@txJust             = "BottomLeft"
  txres@txFont             = "Helvetica"
  txres@txFontHeightF      = 0.013
  txres@txConstantSpacingF = 1.2
  txres@txFontHeightF      = 0.009
  txres@xpos               = 0.1
  subtitle = "McSnow box model data, "+systemfunc("date")

;=================================================;

  if (ltraining .or. ltesting) then
    lplot_actual_lwc = True
    lplot_norm_lwc   = False
    lplot_autconversion = True
    lplot_accretion     = False    
    lplot_cloud_reflectivity = False
    lplot_shape_parameter = False
  else
    if (ltimescales) then
;     turn off plotting
      lplot_actual_lwc = False
      lplot_norm_lwc   = False
      lplot_autconversion = False
      lplot_accretion     = False    
      lplot_cloud_reflectivity = False
      lplot_shape_parameter = False
    else
;     maybe just some plots?     
      lplot_actual_lwc = False
      lplot_norm_lwc   = False
      lplot_autconversion = True
      lplot_accretion     = False    
      lplot_cloud_reflectivity = False
      lplot_shape_parameter = False
    end if
  end if

; actual LWC
  if (lplot_actual_lwc) then
    res2 = res
    res2@tiYAxisString  = "rain water content in "+Lr@units
    res2@tiXAxisString  = "time in "+time@units
    do n=0,ndim-1
      res2@gsnRightString  = "n = "+n
      do j=0,jdim-1
        do k=0,kdim-1
          res2@gsnLeftString  = "r~B~0~N~ = "+rc0(j)+" ~F33~m~F21~m, ~F33~n~F21~~B~0~N~ = "+nu0(k)
          xvar = (/time(n,:,j,k,:)/)
          yvar = (/Lr(n,:,j,k,:)/)
          plot1 = gsn_csm_xy(wks,xvar,yvar,res2)
          gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
          frame(wks)
        end do
      end do
    end do
  end if

; normalized LWC as function of normalized time t*L0  
  if (lplot_norm_lwc) then
    res2 = res
    res2@tiYAxisString  = "normalized liquid water content"
    res2@tiXAxisString  = "normalized time t*L~B~0"
    res2@trYMaxF = 1.01
    res2@trYMinF = 0.0
    res2@trXMaxF = 4.0
    do n=0,ndim-1
      res2@gsnRightString  = "n = "+n
      do j=0,jdim-1
        do k=0,kdim-1
          res2@gsnLeftString  = "r~B~0~N~ = "+rc0(j)+" ~F33~m~F21~m, ~F33~n~F21~~B~0~N~ = "+nu0(k)
          xvar = (/time_norm(n,:,j,k,:)/)
          yvar = (/Lr_norm(n,:,j,k,:)/)
          plot1 = gsn_csm_xy(wks,xvar,yvar,res2)
          gsn_text_ndc(wks,subtitle,txres@xpos,0.01,txres)
          frame(wks)
        end do
      end do
    end do
  end if

; plot as function of timescale tau

  res2 = res
  res2@tiXAxisString  = "liquid water time scale ~F33~t~F21~"
  res2@pmLegendParallelPosF   = 0.20                ; move units right
  res2@pmLegendOrthogonalPosF = -0.42               ; move units down
  res2@trXMinF = 1e-6
  res2@trXMaxF = 1.3
  res2@trYLog  = True
  res2@trXLog  = True
  res2@xyMarkLineMode    = "Markers"
  res2@xyMonoMarker      = True
  res2@xyMarkerSizeF     = 0.005                     ; Marker size (default 0.01)
  plots = new( jdim*kdim, "graphic")

; normalized autoconversion or Phi(tau)
  if (lplot_autconversion) then
    res2@tiYAxisString  = "normalized autconversion ~F33~F~F21~~B~au~N~(~F33~t~F21~)"
    res2@trYMaxF = 700.
    res2@trYMinF = 1e-1
;   plot only one example (usually r=12 and nu=0)
    j = 1
    k = 0
;   SB2001 phi_au
    p = 0.68
    tau2 = 10^(fspan(-7,-1e-3,200))
    phi  = 600 * tau2^p * (1.0 - tau2^p)^3
    print("Plotting "+res2@tiYAxisString+" for r0 = "+rc0(j)+", nu0 = "+nu0(k))
    do n=0,ndim-1
      res2@gsnRightString  = "n = "+n
      res2@xyMarkerColors = colors_lwc
      res2@xyMarker = 16
      res2@gsnLeftString  = "r~B~0~N~ = "+rc0(j)+" ~F33~m~F21~m, ~F33~n~F21~~B~0~N~ = "+nu0(k)
      xvar = (/tau(n,:,j,k,:)/)
      yvar = (/auto_norm(n,:,j,k,:)/)
      xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
      yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
      plot = gsn_csm_xy(wks,xvar,yvar,res2)
      res5 = res2
      res5@pmLegendDisplayMode = "Never"
      res5@xyMarkLineMode := "Lines"
      res5@xyLineColors   := 1
      res5@lgItemOrder    := 0
      res5@xyDashPatterns    := 16
      res5@xyLineThicknesses := 6.
      res5@gsnLeftString  = ""  
      res5@tiYAxisString  = ""
      res5@tiXAxisString  = ""
      plot = gsn_csm_xy(wks,tau2,phi,res5)
      frame(wks)
    end do

;   autoconversion for different nu, at one rc
    res3 = res2
    res3@pmLegendDisplayMode    = "Always"            ; turn on legend
    res3@pmLegendSide           = "Top"               ; Change location of 
    res3@pmLegendParallelPosF   = 0.13                ; move units right
    res3@pmLegendOrthogonalPosF = -0.38               ; move units down
    res3@pmLegendWidthF         = 0.05                ; Change width and
    res3@pmLegendHeightF        = 0.18                ; height of legend.
    res3@xyExplicitLegendLabels := " ~F33~n~F21~~B~0~N~ = "+nu0
    res3@lgItemOrder            := items_nu0
    
    do n=0,ndim-1
      do j=0,0  ;  jdim-1
        print("Plotting "+res2@tiYAxisString+" for r0 = "+rc0(j))
        res4 := res3
        res4@xyMarkerColors = colors_nu
        res4@xyMarker = 16
        res4@gsnRightString = "n = "+n
        res4@gsnLeftString  = "r~B~0~N~ = "+rc0(j)+" ~F33~m~F21~m"
        thin = 1
        do i=0,idim-1
          xvar := (/tau(n,i,j,:,::thin)/)
          yvar := (/auto_norm(n,i,j,:,::thin)/)
          xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
          yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
          plot = gsn_csm_xy(wks,xvar,yvar,res4)
          res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
          res4@tmXBOn         = False  ; the data
          res4@tmXTBorderOn   = False
          res4@tmXTOn         = False
          res4@tmYLBorderOn   = False
          res4@tmYLOn         = False
          res4@tmYRBorderOn   = False
          res4@tmYROn         = False
          res4@pmLegendDisplayMode = "Never" ; turn off legend
          res4@gsnRightString = ""  
          res4@gsnLeftString  = ""  
          res4@tiYAxisString  = ""
          res4@tiXAxisString  = ""
        end do
        res5 = res4
        res5@pmLegendDisplayMode = "Never"
        res5@xyMarkLineMode := "Lines"
        res5@xyLineColors   := 1
        res5@lgItemOrder    := 0
        res5@xyDashPatterns    := 16
        res5@xyLineThicknesses := 6.
        res5@gsnLeftString  = ""  
        res5@tiYAxisString  = ""
        res5@tiXAxisString  = ""
        p = 0.68
        tau2 = 10^(fspan(-7,-1e-3,200))
        phi  = 600 * tau2^p * (1.0 - tau2^p)^3
        plot = gsn_csm_xy(wks,tau2,phi,res5)
        frame(wks)
      end do
    end do

;   autoconversion for different rc, at one mu
    res3 := res2
    res3@pmLegendDisplayMode    = "Always"            ; turn on legend
    res3@pmLegendSide           = "Top"               ; Change location of 
    res3@pmLegendParallelPosF   = 0.15                ; move units right
    res3@pmLegendOrthogonalPosF = -0.34               ; move units down
    res3@pmLegendWidthF         = 0.05                ; Change width and
    res3@pmLegendHeightF        = 0.15                ; height of legend.
    res3@xyExplicitLegendLabels := " r~B~0~N~ = "+rc0+" ~F33~m~F21~m"
    res3@lgItemOrder            := items_rc0
    
    do n=0,ndim-1
      do k=0,0 ; kdim-1
        print("Plotting "+res2@tiYAxisString+" for nu = "+nu0(k))
        res4 := res3
        res4@xyMarkerColors := colors_rc
        res4@xyMarker = 16
        res4@gsnRightString = "n = "+n
        res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
        thin = 1
        do i=0,idim-1
          xvar := (/tau(n,i,:,k,::thin)/)
          yvar := (/auto_norm(n,i,:,k,::thin)/)
          xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
          yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
          plot = gsn_csm_xy(wks,xvar,yvar,res4)
          res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
          res4@tmXBOn         = False  ; the data
          res4@tmXTBorderOn   = False
          res4@tmXTOn         = False
          res4@tmYLBorderOn   = False
          res4@tmYLOn         = False
          res4@tmYRBorderOn   = False
          res4@tmYROn         = False
          res4@pmLegendDisplayMode = "Never" ; turn off legend
          res4@gsnRightString = ""  
          res4@gsnLeftString  = ""  
          res4@tiYAxisString  = ""
          res4@tiXAxisString  = ""
        end do
        res5 = res4
        res5@xyMarkLineMode := "Lines"
        res5@xyLineColors   := 1
        res5@lgItemOrder    := 0
        res5@xyDashPatterns    := 16
        res5@xyLineThicknesses := 6.
        res5@gsnLeftString  = ""  
        res5@tiYAxisString  = ""
        res5@tiXAxisString  = ""
        p = 0.68
        tau2 = 10^(fspan(-7,-1e-3,200))
        phi  = 600 * tau2^p * (1.0 - tau2^p)^3
        plot = gsn_csm_xy(wks,tau2,phi,res5)
        frame(wks)
      end do
    end do

  end if

  if (lplot_accretion) then

;   normalized accretion or Phi_ac(tau)
    res2@tiYAxisString  = "normalized accretion"
    res2@pmLegendParallelPosF   = 0.75                ; move units right
    res2@pmLegendOrthogonalPosF = -1.0               ; move units down
    res2@trYMaxF = 2
    res2@trYMinF = 1e-1

    j = 0
    k = 0
    print("Plotting "+res2@tiYAxisString+" for r0 = "+rc0(j)+", nu0 = "+nu0(k))
    do n=0,ndim-1
      res2@gsnRightString  = "n = "+n
      res2@xyMarker = 16
      res2@gsnLeftString  = "r~B~0~N~ = "+rc0(j)+" ~F33~m~F21~m, ~F33~n~F21~~B~0~N~ = "+nu0(k)
      xvar := (/tau(n,:,j,k,:)/)
      yvar := (/accr_norm(n,:,j,k,:)/)
      xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
      yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
      plot = gsn_csm_xy(wks,xvar,yvar,res2)
      frame(wks)
    end do
    
;   accretion for different rc, at one mu
    res3 := res2
    res3@pmLegendDisplayMode    = "Always"            ; turn on legend
    res3@pmLegendSide           = "Top"               ; Change location of 
    res3@pmLegendParallelPosF   = 0.80                ; move units right
    res3@pmLegendOrthogonalPosF = -1.0                ; move units down
    res3@pmLegendWidthF         = 0.05                ; Change width and
    res3@pmLegendHeightF        = 0.15                ; height of legend.
    res3@xyExplicitLegendLabels := " r~B~0~N~ = "+rc0+" ~F33~m~F21~m"
    res3@lgItemOrder            := items_rc0
    
    do k=0,0  ; kdim-1
      print("Plotting "+res2@tiYAxisString+" for nu = "+nu0(k)+" (log-log axes)")
      do n=0,ndim-1
        res4 := res3
        res4@xyMarkerColors := colors_rc
        res4@xyMarker = 16
        res4@gsnRightString  = "n = "+n
        res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
        thin = 1
        do i=0,idim-1
          xvar := (/tau(n,i,:,k,::thin)/)
          yvar := (/accr_norm(n,i,:,k,::thin)/)
          xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
          yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
          plot = gsn_csm_xy(wks,xvar,yvar,res4)
          res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
          res4@tmXBOn         = False  ; the data
          res4@tmXTBorderOn   = False
          res4@tmXTOn         = False
          res4@tmYLBorderOn   = False
          res4@tmYLOn         = False
          res4@tmYRBorderOn   = False
          res4@tmYROn         = False
          res4@pmLegendDisplayMode = "Never" ; turn off legend
          res4@gsnRightString  = ""  
          res4@gsnLeftString  = ""  
          res4@tiYAxisString  = ""
          res4@tiXAxisString  = ""
        end do
        res5 = res4
        res5@xyMarkLineMode := "Lines"
        res5@xyLineColors   := 1
        res5@lgItemOrder    := 0
        res5@xyDashPatterns    := 16
        res5@xyLineThicknesses := 6.
        res5@gsnLeftString  = ""  
        res5@tiYAxisString  = ""
        res5@tiXAxisString  = ""
        tau2 = 10^(fspan(-7,-1e-3,200))
        phi  = (tau2/(tau2+5e-4))^4  
        plot = gsn_csm_xy(wks,tau2,phi,res5)
        frame(wks)
      end do
    end do
    
    do k=0,0 ; kdim-1
      print("Plotting "+res2@tiYAxisString+" for nu = "+nu0(k)+" (linear axes)")
      do n=0,ndim-1
        res4 := res3
        res4@xyMarkerColors := colors_rc
        res4@xyMarker = 16
        res4@gsnRightString  = "n = "+n
        res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
        res4@trXLog  = False
        res4@trYLog  = False
        res4@trYMaxF = 1.5
        res4@trYMinF = 0.
        res4@trXMaxF = 1.
        res4@trXMinF = 0.
        thin = 1
        do i=0,idim-1
          xvar := (/tau(n,i,:,k,::thin)/)
          yvar := (/accr_norm(n,i,:,k,::thin)/)
          xvar = where(xvar.ge.1e-3,xvar,xvar@_FillValue)  ; taking out some values near tau=0
          yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
          plot = gsn_csm_xy(wks,xvar,yvar,res4)
          res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
          res4@tmXBOn         = False  ; the data
          res4@tmXTBorderOn   = False
          res4@tmXTOn         = False
          res4@tmYLBorderOn   = False
          res4@tmYLOn         = False
          res4@tmYRBorderOn   = False
          res4@tmYROn         = False
          res4@pmLegendDisplayMode = "Never" ; turn off legend
          res4@gsnRightString  = ""  
          res4@gsnLeftString  = ""  
          res4@tiYAxisString  = ""
          res4@tiXAxisString  = ""
        end do
        res5 = res4
        res5@xyMarkLineMode := "Lines"
        res5@xyLineColors   := 1
        res5@lgItemOrder    := 0
        res5@xyDashPatterns    := 16
        res5@xyLineThicknesses := 6.
        res5@gsnLeftString  = ""  
        res5@tiYAxisString  = ""
        res5@tiXAxisString  = ""
        tau2 = 10^(fspan(-7,-1e-3,200))
        phi  = (tau2/(tau2+5e-4))^4  
        plot = gsn_csm_xy(wks,tau2,phi,res5)
        frame(wks)
      end do
    end do
  end if

  if (lplot_cloud_reflectivity) then

;   reflectivity for different rc, at one mu
    res3 := res2
    res3@pmLegendDisplayMode    = "Always"            ; turn on legend
    res3@pmLegendSide           = "Top"               ; Change location of 
    res3@pmLegendParallelPosF   = 0.15                ; move units right
    res3@pmLegendOrthogonalPosF = -0.34               ; move units down
    res3@pmLegendWidthF         = 0.05                ; Change width and
    res3@pmLegendHeightF        = 0.15                ; height of legend.
    res3@xyExplicitLegendLabels := " r~B~0~N~ = "+rc0+" ~F33~m~F21~m"
    res3@lgItemOrder            := items_rc0

    res3@trYLog  = False
    res3@trYMaxF = 2.
    res3@trYMinF = 0.

;   normalized cloud reflectivity    
    res4 := res3
    res4@tiYAxisString  = "normalized cloud reflectivity Z~B~c~N~/Z~B~c,0~N~"

    n = 0
    do k=0,0  ;  kdim-1
      print("Plotting "+res4@tiYAxisString+" for nu = "+nu0(k))
      res4@xyMarkerColors := colors_rc
      res4@xyMarker = 16
      res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
      res4@gsnRightString = "n = "+n
      thin = 1
      do i=0,idim-1
        xvar := (/tau(n,i,:,k,::thin)/)
        yvar := (/Zc_norm(n,i,:,k,::thin)/)
        xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
        yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
        plot = gsn_csm_xy(wks,xvar,yvar,res4)
        res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
        res4@tmXBOn         = False  ; the data
        res4@tmXTBorderOn   = False
        res4@tmXTOn         = False
        res4@tmYLBorderOn   = False
        res4@tmYLOn         = False
        res4@tmYRBorderOn   = False
        res4@tmYROn         = False
        res4@pmLegendDisplayMode = "Never" ; turn off legend
        res4@gsnRightString = ""  
        res4@gsnLeftString  = ""  
        res4@tiYAxisString  = ""
        res4@tiXAxisString  = ""
      end do
      frame(wks)
    end do

;   cloud reflectivity enhancement
    res3@tiYAxisString  = "cloud refl. enhanc. (Z~B~c~N~ - Z~B~c,0~N~)/Z~B~c,0~N~ in %"
    res3@trYMaxF = 50.
    
    do k=0,0  ; kdim-1
      print("Plotting "+res3@tiYAxisString+" for nu = "+nu0(k))
      res4 := res3
      res4@xyMarkerColors := colors_rc
      res4@xyMarker = 16
      res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
      res4@gsnRightString = "n = "+n
      thin = 1
      do i=0,idim-1
        xvar := (/tau(n,i,:,k,::thin)/)
        yvar := (/Zc_enh(n,i,:,k,::thin)/)
        xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
        yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
        plot = gsn_csm_xy(wks,xvar,yvar,res4)
        res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
        res4@tmXBOn         = False  ; the data
        res4@tmXTBorderOn   = False
        res4@tmXTOn         = False
        res4@tmYLBorderOn   = False
        res4@tmYLOn         = False
        res4@tmYRBorderOn   = False
        res4@tmYROn         = False
        res4@pmLegendDisplayMode = "Never" ; turn off legend
        res4@gsnRightString = ""  
        res4@gsnLeftString  = ""  
        res4@tiYAxisString  = ""
        res4@tiXAxisString  = ""
      end do
      res5 = res4
      res5@xyMarkLineMode := "Lines"
      res5@xyLineColors   := 1
      res5@lgItemOrder    := 0
      res5@xyDashPatterns    := 16
      res5@xyLineThicknesses := 6.
      res5@gsnLeftString  = ""  
      res5@tiYAxisString  = ""
      res5@tiXAxisString  = ""
      p = 0.45
      tauC = 0.8
      tau2 = 10^(fspan(-7,log10(0.55),200))
      phiZ = 420 * tau2^p * (tauC - tau2^p)^2
      plot = gsn_csm_xy(wks,tau2,phiZ,res5)
      frame(wks)
    end do

    res3@tiYAxisString  = "reflectivity contribution Z~B~c~N~/(Z~B~c~N~ + Z~B~r~N~)"
    res3@trYMaxF = 1.2

    Zratio = Zc / (Zc+Zr)

    do k=0,0  ;  kdim-1
      print("Plotting "+res3@tiYAxisString+" for nu = "+nu0(k))
      res4 := res3
      res4@pmLegendOrthogonalPosF = -0.99               ; move units down
      res4@xyMarkerColors := colors_rc
      res4@xyMarker = 16
      res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
      thin = 1
      do i=0,idim-1
        xvar := (/tau(n,i,:,k,::thin)/)
        yvar := (/Zratio(n,i,:,k,::thin)/) 
        xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
        yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
        plot = gsn_csm_xy(wks,xvar,yvar,res4)
        res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
        res4@tmXBOn         = False  ; the data
        res4@tmXTBorderOn   = False
        res4@tmXTOn         = False
        res4@tmYLBorderOn   = False
        res4@tmYLOn         = False
        res4@tmYRBorderOn   = False
        res4@tmYROn         = False
        res4@pmLegendDisplayMode = "Never" ; turn off legend
        res4@gsnRightString = ""  
        res4@gsnLeftString  = ""  
        res4@tiYAxisString  = ""
        res4@tiXAxisString  = ""
      end do
      frame(wks)
    end do
        
    res3@tiYAxisString  = "total refl. enhanc. (Z~B~c~N~ - Z~B~c,0~N~)/(Z~B~c~N~ + Z~B~r~N~) in %"
    res3@trYMaxF = 25.

    Zerr = (Zc - conform(Zc,Zc(:,:,:,:,0),(/0,1,2,3/))) / (Zc+Zr)

    do k=0,0  ;  kdim-1
      print("Plotting "+res3@tiYAxisString+" for nu = "+nu0(k))
      res4 := res3
      res4@xyMarkerColors := colors_rc
      res4@xyMarker = 16
      res4@gsnLeftString  = "~F33~n~F21~~B~0~N~ = "+nu0(k)
      res4@gsnRightString = "n = "+n
      thin = 1
      do i=0,idim-1
        xvar := (/tau(n,i,:,k,::thin)/)
        yvar := (/Zerr(n,i,:,k,::thin)/) * 100.
        xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
        yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
        plot = gsn_csm_xy(wks,xvar,yvar,res4)
        res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
        res4@tmXBOn         = False  ; the data
        res4@tmXTBorderOn   = False
        res4@tmXTOn         = False
        res4@tmYLBorderOn   = False
        res4@tmYLOn         = False
        res4@tmYRBorderOn   = False
        res4@tmYROn         = False
        res4@pmLegendDisplayMode = "Never" ; turn off legend
        res4@gsnRightString = ""  
        res4@gsnLeftString  = ""  
        res4@tiYAxisString  = ""
        res4@tiXAxisString  = ""
      end do
      frame(wks)
    end do

  end if

  if (lplot_shape_parameter) then

;   cloud shape parameter for different initial nu, at one rc
    res3 = res2
    res3@pmLegendDisplayMode    = "Always"            ; turn on legend
    res3@pmLegendSide           = "Top"               ; Change location of 
    res3@pmLegendParallelPosF   = 0.82                ; move units right
    res3@pmLegendOrthogonalPosF = -0.42               ; move units down
    res3@pmLegendWidthF         = 0.05                ; Change width and
    res3@pmLegendHeightF        = 0.18                ; height of legend.
    res3@xyExplicitLegendLabels := " ~F33~n~F21~~B~0~N~ = "+sprintf("%4.1f",nu0)
    res3@lgItemOrder            := items_nu0    
    res3@tiYAxisString  = "cloud droplet shape parameter"
    res3@trYMaxF = 4.
    res3@trYMinF = -0.5
    res3@trYLog  = False
    
    do n=0,ndim-1
      do j=1,1 ; 0,jdim-1
        print("Plotting "+res2@tiYAxisString+" for r0 = "+rc0(j))
        res4 := res3
        res4@xyMarkerColors = colors_nu
        res4@xyMarker = 16
        res4@gsnLeftString  = "r~B~0~N~ = "+rc0(j)+" ~F33~m~F21~m"
        res4@gsnRightString = "n = "+n
        thin = 1
        do i=0,idim-1
          xvar := (/tau(n,i,j,:,::thin)/)
          yvar := (/nue(n,i,j,:,::thin)/)
          xvar = where(xvar.gt.0,xvar,xvar@_FillValue)
;         yvar = where(yvar.gt.0,yvar,yvar@_FillValue)
          plot = gsn_csm_xy(wks,xvar,yvar,res4)
          res4@tmXBBorderOn   = False  ; not nice, but I am too lazy to restructure
          res4@tmXBOn         = False  ; the data
          res4@tmXTBorderOn   = False
          res4@tmXTOn         = False
          res4@tmYLBorderOn   = False
          res4@tmYLOn         = False
          res4@tmYRBorderOn   = False
          res4@tmYROn         = False
          res4@pmLegendDisplayMode = "Never" ; turn off legend
          res4@gsnRightString = ""
          res4@gsnLeftString  = ""  
          res4@tiYAxisString  = ""
          res4@tiXAxisString  = ""
        end do
        frame(wks)
      end do
    end do
  end if

;=================================================;

; ensemble coordinate

  ens = ispan(0,ndim-1,1)

; name the coordinates 
    
  lwc0!0 = "ii"
  rc0!0  = "jj"
  nu0!0  = "kk"
  ens!0  = "nn"

  plist = [/ time,tau,Lc,Nc,Zc,Lr,Nr,Zr,auto_num,auto_mass,accr_num,accr_mass,self_cloud,self_rain, \
             Lc_norm,Lr_norm,auto_norm,accr_norm,Zc_norm,Zc_enh,nu4d,mue,nue /]
  do n=0,ListCount(plist)-1 
    plist[n]!0 = "n" 
    plist[n]!1 = "i" 
    plist[n]!2 = "j" 
    plist[n]!3 = "k" 
    plist[n]!4 = "nt" 
    plist[n]&i = lwc0
    plist[n]&j = rc0
    plist[n]&k = nu0
    plist[n]&n = ens
  end do

  plist = [/ t10,t50 /]
  do n=0,ListCount(plist)-1 
    plist[n]!0 = "n" 
    plist[n]!1 = "i" 
    plist[n]!2 = "j" 
    plist[n]!3 = "k" 
    plist[n]&i = lwc0
    plist[n]&j = rc0
    plist[n]&k = nu0
    plist[n]&n = ens
  end do

; make sure we have proper variable names

  time@long_name = "time"

  lwc0@long_name = "Initial liquid water content"
  rc0@long_name  = "Initial radius of cloud droplets"
  nu0@long_name  = "Initial gamma shape parameter of cloud droplets"
  ens@long_name  = "ensemble number"
  
  Lc@long_name = "cloud water content"
  Nc@long_name = "number density of cloud droplets"
  Zc@long_name = "reflectivity of cloud droplets"
  Lr@long_name = "rain water content"
  Nr@long_name = "number density of raindrops"
  Zr@long_name = "reflectivity of raindrops"

  tau@long_name = "liquid water time scale"

  mue@long_name  = "gamma shape of raindrop distribution f(D)"
  nue@long_name  = "gamma shape of cloud droplet distribution f(x)"

  nu4d@long_name = "initial shape of cloud droplet distribution"

  auto_norm@long_name = "normalized mass autoconversion rate Phi_au(tau)"
  accr_norm@long_name = "normalized mass accretion rate Phi_ac(tau)"

; set some units (all others are SI)

  plist = [/ Lc, Lr/]
  do n=0,ListCount(plist)-1
    if  (plist[n]@units.eq."g/m~S~3".or.plist[n]@units.eq."g/m3") then
      plist[n] = plist[n] * 1e-3
      plist[n]@units = "kg m-3"
    end if
  end do

  plist = [/ Nc, Nr/]
  do n=0,ListCount(plist)-1 
    plist[n]@units = "m-3"
  end do

  plist = [/ Zc, Zr/]
  do n=0,ListCount(plist)-1 
    plist[n]@units = "kg2 m-3"
  end do

  t10@long_name = "time scale of conversion of 10 % of cloud water to rain"
  t50@long_name = "time scale of conversion of 50 % of cloud water to rain"
  t10@units = "s"
  t50@units = "s"

; set units of dimensionless quantities to 1 (in accordance to CF convention)

  plist = [/ tau,Lc_norm,Lr_norm,auto_norm,accr_norm,Zc_norm,mue,nue,nu4d /]
  do n=0,ListCount(plist)-1 
    plist[n]@units = "1" 
  end do

;  auto_norm = 400 * tau^0.65 * (1.0 - tau^0.65)^3
 
  xc = Lc
  print(" min Lc = "+min(xc)+" max Lc = "+max(xc))
  xc = log(Lc)
  print(" min log(Lc) = "+min(xc)+" max log(Lc) = "+max(xc))

  xc = tau
  print(" min tau = "+min(xc)+" max tau = "+max(xc))
  xc = log(tau)
  print(" min log(tau) = "+min(xc)+" max log(tau) = "+max(xc))

  xc = Nc
  print(" min Nc = "+min(xc)+" max Nc = "+max(xc))
  xc = log(Nc)
  print(" min log(Nc) = "+min(xc)+" max log(Nc) = "+max(xc))

  xc = Lc/Nc
  print(" min xc = "+min(xc)+" max xc = "+max(xc))
  xc = log(xc)
  print(" min log(xc) = "+min(xc)+" max log(xc) = "+max(xc))

  print(" number of non-missing accr_mass = "+num(.not.ismissing(accr_mass)))
  print(" number of non-missing mue = "+num(.not.ismissing(mue)))

;=================================================;

  if (lwrite_full_netcdf) then
    fname  = "autocon"+pfix+".nc"
    outcdf =  basedir+"/"+fname
    print("Writing data to netCDF file: "+outcdf)
    system("/bin/rm -f "+outcdf)
    ncdf     = addfile(outcdf,"c")  ; open output netCDF file
    ncdf@title     = "McSnow warm-rain output pre-processed by NCL"
    ncdf@date      = systemfunc("date")    ; the netCDF file.
    ncdf@basedir   = basedir
    ncdf@asciifile = filename
    ncdf@xi0       = xi 

    ncdf->time = time
    ncdf->tau  = tau

    ncdf->Lc = Lc
    ncdf->Nc = Nc
    ncdf->Zc = Zc
    ncdf->Lr = Lr
    ncdf->Nr = Nr
    ncdf->Zr = Zr
    ncdf->nu = nu4d

    ncdf->mue = mue
    ncdf->nue = nue

    ncdf->auto_num  = auto_num
    ncdf->auto_mass = auto_mass
    ncdf->accr_num  = accr_num
    ncdf->accr_mass = accr_mass
    ncdf->self_cloud = self_cloud
    ncdf->self_rain = self_rain
    ncdf->Lc_norm   = Lc_norm
    ncdf->Lr_norm   = Lr_norm
    ncdf->auto_norm = auto_norm
    ncdf->accr_norm = accr_norm

    ncdf->Zc_norm = Zc_norm
    ncdf->Zc_enh  = Zc_enh

    ncdf->t10 = t10
    ncdf->t50 = t50

;   one-dimensional coordinates
    ncdf->lwc0 = lwc0
    ncdf->rc0 = rc0
    ncdf->nu0 = nu0
    ncdf->ens = ens

  end if

  if (ltimescales) then
    fname  = "autocon"+pfix+".nc"
    outcdf =  basedir+"/"+fname
    print("Writing data to netCDF file: "+outcdf)
    system("/bin/rm -f "+outcdf)
    ncdf     = addfile(outcdf,"c")  ; open output netCDF file
    ncdf@title     = "McSnow warm-rain output pre-processed by NCL"
    ncdf@date      = systemfunc("date")    ; the netCDF file.
    ncdf@basedir   = basedir
    ncdf@asciifile = filename
    ncdf@xi0       = xi 

    ncdf->t10 = t10
    ncdf->t50 = t50

;   some process rates
    ncdf->auto_mass  = auto_mass
    ncdf->accr_mass  = accr_mass
    ncdf->self_cloud = self_cloud
    ncdf->self_rain  = self_rain

;   one-dimensional coordinates
    ncdf->lwc0 = lwc0
    ncdf->rc0 = rc0
    ncdf->nu0 = nu0
    ncdf->ens = ens

    ncdf->time = time(0,0,0,0,:)
  end if

;=================================================;
  print("Finished "+fodir+foname+pfix+"."+foform)
;=================================================;

  end

