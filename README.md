# WarmRainML

Machine learning is applied to the parameterization of warm-rain bulk microphysics. The results are described and discussed in

Axel Seifert and Stephan Rasp, 2020, **Potential and limitations of
machine learning for modeling warm-rain cloud microphysical
processes**, *J. Adv. Modeling Earth
Systems*, 12, [https://doi.org/10.1029/2020MS002301](https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2020MS002301)

The following Python notebooks are used in the paper

[Basic autoconversion notebook using Tensorflow 2.1](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_autocon_v9_01_eps15_z12_sigm_tf2.1.ipynb)

This notebook contains the basic ML models for autoconversion. It
creates Figs. 2, 3, and 4 of the paper as well as the results for MAE
and MSE for the testing data as given in Table 2. 

Using Tensorflow 2.2 gives slightly different numbers, but qualitatively the same results

[Basic autoconversion notebook using Tensorflow 2.2](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_autocon_v9_01_eps15_z12_sigm.ipynb)

Similar notebooks apply ML to
[accretion](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_accretion_v9_01_eps15_sigm.ipynb)
to [self-collection of cloud
droplets](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_selfcollCloud_v9_01_eps15_sigm.ipynb)
and [self-collection of
raindrops](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_selfcollRain_v9_01_eps15_sigm.ipynb). Those
three notebooks together produce Figure 5 of the paper.

The partial-dependency analysis is done in [PDP
notebook](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/PDP_plots.ipynb)
and the result is Figure 6 of the paper.

The ODE system that describes the collision-coalescence of cloud
droplets and raindrops is solved in [ODE
notebook](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/SolveWarmRainODE_19_dt5.ipynb)
and contains Figures similar to Figs. 7, 8 and 9 of the paper. Note
that the actual Figures of the paper have been created with NCL (NCAR
command language) and are not part of these Python notebooks.

The directory contains various other notebooks, e.g., sensitivity
studies with different activation functions like,
[ReLU](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_autocon_v9_01_eps15_z12_ReLU.ipynb)
or
[PReLU](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_autocon_v9_01_eps15_z12_PReLU.ipynb),
or different
[size](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_autocon_v9_01_eps15_z12_ReLU_deep_wide.ipynb)
of the neural net etc. that are described in the Supplement of the
paper. 

In addition, some notebooks implement ML models that explicitly
predict the number change due to
[autoconversion](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_autocon_withN_v9_02_eps15_z12_sigm.ipynb)
and
[accretion](https://gitlab.com/axelseifert/warmrain/-/blob/master/notebooks/McSnow_accretion_withN_v9_02_eps15_sigm.ipynb).

The basic Python notebooks, all necessary NCL scripts and the training data can be downloaded from the Zenodo archive

[Data and scripts for 'Potential and limitations of machine learning for modeling warm-rain cloud microphysical processes'](https://zenodo.org/record/3988974#.X66xTpNKihd)

The training data is based on benchmark solutions of the
collision-coalesence equation, which have been performed using a
[Monte-Carlo particle
model](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2017MS001167),
which implements the super-droplet algorithm of [Shima et
al. 2008](https://rmets.onlinelibrary.wiley.com/doi/abs/10.1002/qj.441).

